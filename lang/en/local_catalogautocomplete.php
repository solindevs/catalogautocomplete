<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local_catalogautocomplete
 * @copyright  2021 Ahmed Landolsi
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Catalog auto complete';
$string['pluginname_desc'] = 'Auto complete text for the search field in the Totara catalog grid.';

$string['completefrommetadata'] = 'Search metadata:';
$string['completefrommetadata_desc'] = 'Autocomplete the search text with text from search metadata.';
$string['completefromtags'] = 'Standard tags:';
$string['completefromtags_desc'] = 'Autocomplete the search text with text from standard tags.';
$string['isenabled'] = 'Enable Keywork search automation:';
$string['isenabled_desc'] = '';
$string['itemsnumber'] = 'Maximum text matches to display:';
$string['itemsnumber_desc'] = '';
$string['scopesheader'] = 'Scopes settings';
$string['settingsheader'] = 'Autocompletion settings';
$string['startafter'] = 'Number of characters to begin word search:';
$string['startafter_desc'] = '';


