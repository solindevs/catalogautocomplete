<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_catalogautocomplete
 * @copyright  2021 Ahmed Landolsi
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) {
    $settings = new admin_settingpage('local_catalogautocomplete', get_string('pluginname', 'local_catalogautocomplete'));
    
    $settings->add(new admin_setting_heading('local_catalogautocomplete/settingsheader', get_string('settingsheader', 'local_catalogautocomplete'), ''));
    
    $settings->add(new admin_setting_configcheckbox('local_catalogautocomplete/isenabled',
        new lang_string('isenabled', 'local_catalogautocomplete'),
        new lang_string('isenabled_desc', 'local_catalogautocomplete'), ''));
    
    $char_count_menu = array('1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9, '10' => 10);
    $settings->add(new admin_setting_configselect('local_catalogautocomplete/startafter',
        new lang_string('startafter', 'local_catalogautocomplete'),
        new lang_string('startafter_desc', 'local_catalogautocomplete'), '3', $char_count_menu));
    
    $match_items_count_menu = array('3' => 3, '5' => 5, '10' => 10, '15' => 15, '20' => 20);
    $settings->add(new admin_setting_configselect('local_catalogautocomplete/itemsnumber',
        new lang_string('itemsnumber', 'local_catalogautocomplete'),
        new lang_string('itemsnumber_desc', 'local_catalogautocomplete'), '10', $match_items_count_menu));
    
    $settings->add(new admin_setting_heading('local_catalogautocomplete/scopesheader', get_string('scopesheader', 'local_catalogautocomplete'), ''));
    
    $settings->add(new admin_setting_configcheckbox('local_catalogautocomplete/completefromtags',
        new lang_string('completefromtags', 'local_catalogautocomplete'),
        new lang_string('completefromtags_desc', 'local_catalogautocomplete'), ''));
    
    $settings->add(new admin_setting_configcheckbox('local_catalogautocomplete/completefrommetadata',
        new lang_string('completefrommetadata', 'local_catalogautocomplete'),
        new lang_string('completefrommetadata_desc', 'local_catalogautocomplete'), ''));
    
    $ADMIN->add('localplugins', $settings);    
}







