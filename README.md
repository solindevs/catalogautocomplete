# Information

Plugin name: catalogautocomplete <br/>
Plugin typ: local <br/>
Totara version: 13.3 <br/>

# Installation

Install plugin: Copy plugin files in /server/local/catalogautocomplete/  and apply the following two core changes:

## Change in the mustache template

In file /server/totara/core/templates/select_search_text.mustache, add this after comments and before template html.


```mustache

{{! SOLIN changes Begin }}
{{#local_catalogautocomplete_startafter}}
{{#js}}
require(['local_catalogautocomplete/catalogAutocompleteTag'], function(Autocomplete) {
    Autocomplete.init({{local_catalogautocomplete_startafter}});
});
{{/js}}
{{/local_catalogautocomplete_startafter}}
{{! SOLIN changes End }}
```

## Change in the catalog output class

In file /server/totara/catalog/classes/output/catalog.php, add this after line 221 ('case "totara_core/select_search_text":')


```php

// SOLIN changes Begin
global $PAGE;
if ((1 == get_config('local_catalogautocomplete', 'isenabled')) && ($PAGE->url->get_path() == '/totara/catalog/index.php')) {
    $filter->template_data['local_catalogautocomplete_startafter'] = get_config('local_catalogautocomplete', 'startafter');
}
// SOLIN changes End
```
