<?php

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    local_catalogautocomplete
 * @copyright  2021 Ahmed Landolsi
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * 
 */

require_once('../../../config.php');
global $DB, $PAGE;
defined('MOODLE_INTERNAL') || die;

$context = context_system::instance();
$PAGE->set_context($context);
require_login();
require_sesskey();
$query = optional_param('q', '', PARAM_TEXT);
$resultsount = get_config('local_catalogautocomplete', 'itemsnumber');
$completefromtags = (get_config('local_catalogautocomplete', 'completefromtags') == 1) ? true : false;
$completefrommetadata = (get_config('local_catalogautocomplete', 'completefrommetadata') == 1) ? true : false;
$tags = array();
$results = array();
try {
    if ($completefromtags && $completefrommetadata) {
        $metastrings = $DB->get_records_sql('SELECT value FROM {catalog_search_metadata} WHERE ' . $DB->sql_like('value', ':value', false, false), [
            'value' => '%' . $DB->sql_like_escape($query) . '%'
        ], 0, $resultsount / 2);
        if (count($metastrings) < $resultsount / 2) {
            $tagstrings = $DB->get_records_sql('SELECT name FROM {tag} WHERE ' . $DB->sql_like('name', ':name', false, false), [
                'name' => '%' . $DB->sql_like_escape($query) . '%'
            ],0, $resultsount);
        } else {
            $tagstrings = $DB->get_records_sql('SELECT name FROM {tag} WHERE ' . $DB->sql_like('name', ':name', false, false), [
                'name' => '%' . $DB->sql_like_escape($query) . '%'
            ],0, $resultsount / 2);
            if (count($tagstrings) < $resultsount / 2) {
                $metastrings = $DB->get_records_sql('SELECT value FROM {catalog_search_metadata} WHERE ' . $DB->sql_like('value', ':value', false, false), [
                    'value' => '%' . $DB->sql_like_escape($query) . '%'
                ], $resultsount / 2, $resultsount);
            }
        }
        foreach ($metastrings as $value) {
            $csvarray = explode(' ', $value->value);
            $csvarray = array_map('trim',$csvarray);
            $metaarray = array_combine($csvarray, $csvarray);
            // filter array one more time with query string, because sql statment gives back a csv string
            $input = preg_quote($query, '~');
            $metaarray = preg_grep('~' . $input . '~', $metaarray);
            $tags = array_merge($tags, $metaarray);
        }
        foreach ($tagstrings as $value) {
            $csvarray = explode(',', $value->name);
            $csvarray = array_map('trim',$csvarray);
            $tagsarray = array_combine($csvarray, $csvarray);
            $tags = array_merge($tags, $tagsarray);
        }        
    }else if ($completefromtags && !$completefrommetadata) {
        $tagstrings = $DB->get_records_sql('SELECT name FROM {tag} WHERE ' . $DB->sql_like('name', ':name', false, false), [
            'name' => '%' . $DB->sql_like_escape($query) . '%'
        ],0, $resultsount);
        foreach ($tagstrings as $value) {
            $csvarray = explode(',', $value->name);
            $csvarray = array_map('trim',$csvarray);
            $tagsarray = array_combine($csvarray, $csvarray);
            $tags = array_merge($tags, $tagsarray);
        }
    }else if (!$completefromtags && $completefrommetadata) {
        $metastrings = $DB->get_records_sql('SELECT value FROM {catalog_search_metadata} WHERE ' . $DB->sql_like('value', ':value', false, false), [
            'value' => '%' . $DB->sql_like_escape($query) . '%'
        ],0, $resultsount);
        foreach ($metastrings as $value) {
            $csvarray = explode(' ', $value->value);
            $csvarray = array_map('trim',$csvarray);
            $metaarray = array_combine($csvarray, $csvarray);
            // filter array one more time with query string, because sql statment gives back a csv string
            $input = preg_quote($query, '~');            
            $metaarray = preg_grep('~' . $input . '~', $metaarray);            
            $tags = array_merge($tags, $metaarray);
        }
    }
    $results = array_keys($tags);
    $results = array_unique($results, SORT_STRING);
    sort($results);
    $results = array_slice($results, 0, $resultsount);
    echo json_encode($results);    
} catch (Exception $exception) {
    echo json_encode($exception);
}
exit();


