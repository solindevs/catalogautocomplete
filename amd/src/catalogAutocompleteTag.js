define(
        [ 'jquery', 'core/config', 'core/notification' ],
        function ($, mdlcfg, Notification) {
            return {
                init : function (startafter) {
                    var ghandlerurl = mdlcfg.wwwroot + '/local/catalogautocomplete/actions/getalltags.php';
                    var gkeys = [];
                    var gstartafter = startafter;
                    function gettags(query) {
                        if(!query || query.length === 0){
                        }else if (query.length >= gstartafter){
                            $.ajax({
                                url : ghandlerurl,
                                method : 'POST',
                                data: {q: query, sesskey: mdlcfg.sesskey},
                                success : function (result) {
                                    var res = JSON.parse(result);
                                    gkeys = res;
                                },
                                error : function (error) {
                                    Notification.exception(error);
                                }
                            }).done(function() {
                                autocomplete(document.getElementById("catalog_fts_input"),gkeys);
                            });
                        }
                    }
                    function autocomplete(inp, arr) {
                        var currentFocus;
                        showAutocompleteOverlay(inp);
                        inp.addEventListener("input", function (e) {
                            showAutocompleteOverlay(this);
                        });
                        inp.addEventListener("keydown", function (e) {
                            var elementslist = document.getElementById(this.id + "local-catalog-autocomplete-list");
                            if (elementslist) {
                                elementslist = elementslist.getElementsByTagName("div");
                            }
                            if (e.keyCode == 40) {
                                currentFocus++;
                                addActive(elementslist);
                            } else if (e.keyCode == 38) {
                                currentFocus--;
                                addActive(elementslist);
                            } else if (e.keyCode == 13) {
                                e.preventDefault();
                                if (currentFocus > -1) {
                                    if (elementslist) {
                                        elementslist[currentFocus].click();
                                    }
                                }
                            }
                        });
                        function addActive(elementslist) {
                            if (!elementslist) {
                                return false;
                            }
                            if (currentFocus >= elementslist.length) {
                                currentFocus = 0;
                            }
                            if (currentFocus < 0) {
                                currentFocus = (elementslist.length - 1);
                            }
                            if (Number.isInteger(currentFocus)) {
                                removeActive(elementslist);
                                elementslist[currentFocus].classList.add("local-catalog-autocomplete-active");
                                if(elementslist[currentFocus].offsetTop < elementslist[currentFocus].parentNode.scrollTop) {
                                    elementslist[currentFocus].parentNode.scrollTop = elementslist[currentFocus].offsetTop;
                                } else if(elementslist[currentFocus].offsetTop > (elementslist[currentFocus].parentNode.scrollTop + elementslist[currentFocus].parentNode.clientHeight) - elementslist[currentFocus].clientHeight) {
                                    elementslist[currentFocus].parentNode.scrollTop = elementslist[currentFocus].offsetTop - (elementslist[currentFocus].parentNode.clientHeight - elementslist[currentFocus].clientHeight);
                                }
                            }
                        }
                        function removeActive(elementslist) {
                            for (var i = 0; i < elementslist.length; i++) {
                                elementslist[i].classList.remove("local-catalog-autocomplete-active");
                            }
                        }
                        function closeAllLists(elmnt) {
                            var elementslist = document.getElementsByClassName("local-catalog-autocomplete-items");
                            for (var i = 0; i < elementslist.length; i++) {
                                if (elmnt != elementslist[i] && elmnt != inp) {
                                    elementslist[i].parentNode.removeChild(elementslist[i]);
                                }
                            }
                        }
                        function showAutocompleteOverlay(elmnt) {
                            var a, b, i, val = elmnt.value;
                            closeAllLists();
                            if (!val) {
                                return false;
                            }
                            if (!arr) {
                                return false;
                            }
                            currentFocus = -1;
                            a = document.createElement("DIV");
                            a.setAttribute("id", elmnt.id + "local-catalog-autocomplete-list");
                            a.setAttribute("class", "local-catalog-autocomplete-items");
                            elmnt.parentNode.appendChild(a);
                            for (i = 0; i < arr.length; i++) {
                                if ((arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) && (val.length >= gstartafter)) {
                                    b = document.createElement("DIV");
                                    b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                                    b.innerHTML += arr[i].substr(val.length);
                                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                                    b.addEventListener("click", function (e) {
                                        inp.value = this.getElementsByTagName("input")[0].value;
                                        closeAllLists();
                                    });
                                    a.appendChild(b);
                                }
                            }
                            if (a.childElementCount > 6) {
                                a.classList.add("local-catalog-autocomplete-scroll");
                            }else {
                                a.classList.remove("local-catalog-autocomplete-scroll");
                            }
                        }
                        document.addEventListener("click", function (e) {
                            closeAllLists(e.target);
                        });
                    }
                    $('#catalog_fts_input').on("input", function () {
                        if (this.value.length >= gstartafter) {
                            gettags(this.value);
                        }
                     });
                }
            };
        });